/**Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones. 

        - El programa debe recibir dos números (n1, n2).

        - Debe existir una variable que permita seleccionar de alguna forma el tipo de operación (suma, resta, multiplicación 
          o división).

        - Opcional: agrega una operación que permita elevar n1 a la potencia n2.
 */





/**
 * NOTA: ejercicio incompleto. No se me ocurre la manera de que una variable pueda valer sumar o restar o multiplicar o dividir (he probado con objeto y switch).
 * Además no hay que hacer un bucle para que tome un valor u otro, sino que se lo tendríamos que pedir nosotros (por eso en los parámetros he añadido 
 * operacion aunque no se pedía así). ??
 * 
 */



function calculaNumero(num1, num2,operacion) {

    /** let operacion = {
        a: suma,        
        b: resta,
        c: multiplicacion,
        d:division
               
    }
    */

    /** let operacion = {
        suma: function suma(num1, num2) {
            return (parseInt(num1) + parseInt(num2));
        },
        resta: function resta(num1, num2) {
            return (parseInt(num1) - parseInt(num2))
        },
        multiplicacion: function multiplicacion (num1, num2) {
            return (parseInt(num1) * parseInt(num2))
        },
        division: function division(num1, num2) {
            return (parseInt(num1) / parseInt(num2))
        },
    }
    */

   /** 

    if (operacion.suma) {
        function suma(num1, num2) {
            return (parseInt(num1) + parseInt(num2))
        }
    } else if (operacion.resta) {
        function resta(num1, num2) {
            return (parseInt(num1) - parseInt(num2))
        }
    } else if (operacion.multiplicacion) {
        function multiplicacion(num1, num2) {
            return (parseInt(num1) * parseInt(num2))

        }
    } else if (operacion.division) {
        function division(num1, num2) {
            return (parseInt(num1) / parseInt(num2))
        }
    }

    */

    

   switch(operacion) {
       case 'sumar':
        function suma(num1, num2) {
            return (parseInt(num1) + parseInt(num2))
        }
        break;
        case 'restar':
        function resta(num1, num2) {
            return (parseInt(num1) - parseInt(num2))
        }
        break;
        case 'multiplicar':
        function multiplicacion(num1, num2) {
            return (parseInt(num1) * parseInt(num2))

        }
        break;
        case 'dividir':
        function division(num1, num2) {
            return (parseInt(num1) / parseInt(num2))
        }
    }

    return calculaNumero

}



console.log(calculaNumero(3, 5,'sumar'))