/**
 * En este ejercicio se comprobarÃ¡ la competencia de los alumnos en el concepto de asincronÃ­a 
    Se proporcionan 3 archivos  csv separados por comas y se deberÃ¡n bajar asÃ­ncronamente (promises) 
    
    A la salida se juntarÃ¡n los registros de los 3 archivos en un array que serÃ¡ el parÃ¡metro de entrada 
    de la funcion findIPbyName(array, name ,surname) que buscarÃ¡ una entrada en el array y devolverÃ¡ la IP correspondiente

    Una vez hallada la IP ha de mostrarse por pantalla

    para llamar a la funciÃ³n utilizad el nombre Cari Wederell
    
 */





 /**
  * NOTAS: 
  * Al pedir asincronía pensé en axios.get y promises all pero los archivos hay que descargarlos y obtenerlos de la red. Por eso usé fs aunque realmente es
  * asíncrona. 
  * Además el código no funciona, hay algo que está mal. Pero por más que lo busco, no lo veo.
  */
 



const fs = require ('fs')

const file1 = fs.readFileSync('/home/hab23/Documents/development/mod2-pruebatecnica/files/MOCK_DATA1.csv').toString().split('\n');

const file2 = fs.readFileSync ('/home/hab23/Documents/development/mod2-pruebatecnica/files/MOCK_DATA2.csv').toString().split('\n');

const file3 = fs.readFileSync ('/home/hab23/Documents/development/mod2-pruebatecnica/files/MOCK_DATA3.csv').toString().split('\n');

const allFiles = file1.concat(file2, file3)



function findIPbyName(array, name, surname) {

    const NAME_POSITION = 1
    const SURNAME_POSITION = 2
    const IP_POSITION = 5


    for (let line of array) {

        const fieldsOfLine = line.split(",")

        //console.log(fieldsOfLine)


        if (fieldsOfLine[NAME_POSITION].toUpperCase() == name && fieldsOfLine[SURNAME_POSITION].toUpperCase() == surname) {
            break
            console.log(fieldsOfLine[IP_POSITION])
        }

    }

    return (fieldsOfLine[IP_POSITION])


}


const ipBuscada = findIPbyName(allFiles, Cari, Wederell)

console.log(ipBuscada)
